import { Form, useActionData } from "@remix-run/react";
import type { ActionArgs } from "@remix-run/node";
import { createPost } from "~/models/post.server";
import { json, redirect } from "@remix-run/node";
import invariant from "tiny-invariant";
import { useLoaderData } from "@remix-run/react";

import { getPosts } from "~/models/post.server";

//style
const inputClassName = `w-full rounded border border-gray-500 px-2 py-1 text-lg`;

export const loader = async () => {
  return json({ posts: await getPosts() });
};

//find errors
// const findErrors = (post) => {
//   const errors = {
//     title: post ? (post === "a" ? "same title" : null) : "Title is required",
//     slug: post ? null : "Slug is required",
//     markdown: post ? null : "Markdown is required",
//   };
//   const hasErrors = Object.values(errors).some((errorMessage) => errorMessage);
//   if (hasErrors) {
//     return json(errors);
//   }

// }

//connection to backend
export const action = async ({ request }: ActionArgs) => {
  const formData = await request.formData();

  const title = formData.get("title");
  const slug = formData.get("slug");
  const markdown = formData.get("markdown");

  const errors = {
    title: title ? (title === "a" ? "same title" : null) : "Title is required",
    slug: slug ? null : "Slug is required",
    markdown: markdown ? null : "Markdown is required",
  };
  const hasErrors = Object.values(errors).some((errorMessage) => errorMessage);
  if (hasErrors) {
    return json(errors);
  }

  //return only string
  invariant(typeof title === "string", "title must be a string");
  invariant(typeof slug === "string", "slug must be a string");
  invariant(typeof markdown === "string", "markdown must be a string");

  await createPost({ title, slug, markdown });

  return redirect("/posts/admin");
};

//html
export default function NewPost() {
  //to return error
  const errors = useActionData<typeof action>();
  //data from db of posts
  const { posts } = useLoaderData<typeof loader>();

  // findErrors(posts);

  return (
    <Form method="post">
      <p>
        <label>
          Post Title:{" "}
          {errors?.title ? (
            <em className="text-red-600">{errors.title}</em>
          ) : null}
          <input type="text" name="title" className={inputClassName} />
        </label>
      </p>
      <p>
        <label>
          Post Slug:{" "}
          {errors?.slug ? (
            <em className="text-red-600">{errors.slug}</em>
          ) : null}
          <input type="text" name="slug" className={inputClassName} />
        </label>
      </p>
      <p>
        <label htmlFor="markdown">
          Markdown:
          {errors?.markdown ? (
            <em className="text-red-600">{errors.markdown}</em>
          ) : null}
        </label>
        <br />
        <textarea
          id="markdown"
          rows={20}
          name="markdown"
          className={`${inputClassName} font-mono`}
        />
      </p>
      <p className="text-right">
        <button
          type="submit"
          className="rounded bg-blue-500 py-2 px-4 text-white hover:bg-blue-600 focus:bg-blue-400 disabled:bg-blue-300"
        >
          Create Post
        </button>
      </p>
    </Form>
  );
}
