import React from "react";
import type { LoaderArgs } from "@remix-run/node";
import { json } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import { getPost } from "~/models/post.server";
// import type { Post } from "~/models/post.server";
import { marked } from "marked";
import invariant from "tiny-invariant";

// type LoaderData = {post: Post};
//mette loader: LoaderData

export const loader = async ({ params }: LoaderArgs) => {
  invariant(params.slug, `params.slug is required`);

  const post = await getPost(params.slug);

  //nel caso il post non ci sia
  invariant(post, `Post not found: ${params.slug}`);

  const html = marked(post.markdown);

  // <LoaderData>
  return json({ post, html });
};

export default function PostSlug() {
  // <LoaderData>
  const { post, html } = useLoaderData<typeof loader>();

  return (
    <main className="mx-auto max-w-4xl">
      <h1 className="my-6 border-b-2 text-center text-3xl"> {post?.title} </h1>
      <p className="text-center font-semibold">{post.markdown}</p>
      <div className="text-center" dangerouslySetInnerHTML={{ __html: html }} />
    </main>
  );
}
