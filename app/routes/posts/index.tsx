import React from "react";
import { json } from "@remix-run/node";
import { Link, useLoaderData } from "@remix-run/react";

import { getPosts } from "../../models/post.server";

type LoaderData = {
  posts: Awaited<ReturnType<typeof getPosts>>;
};

export const loader = async () => {
  return json<LoaderData>({
    posts: await getPosts(),
  });
};

export default function Posts() {
  const { posts } = useLoaderData<typeof loader>();

  console.log(posts);
  return (
    <main>
      <div className="flex flex-col items-center justify-center">
        {posts.map((i) => {
          return (
            <Link
              key={i.slug}
              to={i.slug}
              className="rounded-md p-2 text-center text-[22px] font-bold shadow-md"
            >
              {i.title}
            </Link>
          );
        })}
        <Link
          to="admin"
          className="rounded-md p-2 text-center text-[22px] font-bold shadow-md"
        >
          Admin
        </Link>
      </div>
    </main>
  );
}
